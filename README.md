Copyright 2014 Carlos III de Madrid University
Copyright 2014 Moises Martinez 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Experimental code to connect PELEA with ros using rosjava. 

How to use:

1. Download source code 
2. Compile source code using java version 1.6 or higher. 
3. Create rospelea directory.
4. Move lib directory into rospelea directory.
5. Move jar generated after compile source code into rospelea directory. 
6. Move configurationRos.xml file into rospelea directory. If you want, you can modify configuration Pelea for node EXE. For more info see http://plg.inf.uc3m.es/pelea. 
7. Move rospelea script into rospelea directory. 
8. Execute rospelea script. ./pelearos org.pelea.ros.ExecutionBasic

If you want you can use a compile version from peleaRos.tar.gz. 

For more info or report errors, send a email to momartin@inf.uc3m.es 