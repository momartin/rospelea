/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.ros.response;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.pelea.utils.Util;

/**
 *
 * @author moises
 */
public class response 
{
    private List<node> nodes; 
    
    public response()
    {
        this.nodes = new ArrayList<node>();
    }
    
    public response(String content)
    {
        String xml = "";
        this.nodes = new ArrayList<node>();

        try
        {
            XMLOutputter outputter  = new XMLOutputter(Format.getPrettyFormat());
            SAXBuilder builder      = new SAXBuilder();
            Document document       = (Document) builder.build(new StringReader(content));
            List elements           = document.getRootElement().getChildren();
            
            //TODO: It should be recursive, but nodes always are in the first level.
            
            for (int i = 0; i < elements.size(); i++)
            {
                this.nodes.add(new node(((Element) elements.get(i)).getName(), outputter.outputString((Element) elements.get(i))));
            }           
         }
         catch (Exception ex)
         {
            Util.printError("Error processing response", ex.toString());
         }
    }
    
    public String getNode(String tag, boolean head)
    {
        boolean find = false;
        int position = 0;
        
        while (position < this.nodes.size())
        {
            if (this.nodes.get(position).getName().matches(tag))
                return this.nodes.get(position).getNode(head);
            
            position++;
        }
        
        return null;
    }
    
    public void addNode(String name, String content) throws JDOMException, IOException
    {
        this.nodes.add(new node(name, content));
    }
    
    public String generateResponse()
    {
        String xml;
        
        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
        xml += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
        
        for (int i = 0; i < this.nodes.size(); i++) 
        {
            xml += this.nodes.get(i).getNode(false);
        } 
        
        xml += "</define>";
        
        return xml;
    }
}
