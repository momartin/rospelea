/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.ros.response;

import java.io.IOException;
import java.io.StringReader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author moises
 */
public class node 
{
    private final String node;
    private final String name;
    
    public node(String name, String node) throws JDOMException, IOException
    {
        this.name = name;
        
        XMLOutputter outputter  = new XMLOutputter(Format.getPrettyFormat());
        SAXBuilder builder      = new SAXBuilder();
        Document document = (Document) builder.build(new StringReader(node));
        
        if (document.getRootElement().getName().toUpperCase().contains("DEFINE"))
            this.node = outputter.outputString((Element) document.getRootElement().getChildren().get(0));
        else
            this.node = outputter.outputString(document.getRootElement());
    }
    
    public String getNode(boolean head)
    {
        String xml;
        
        if (head)
        {
            xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            xml += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            xml += this.node;
            xml += "</define>";
        }
        else 
        {
            xml = this.node;
        }
    
        return xml;
    }
    
    public String getName()
    {
        return this.name;
    }
}
