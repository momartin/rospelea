/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pelea.ros;

import java.io.StringReader;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.pelea.core.communication.Message;
import org.pelea.core.communication.Messages;
import org.pelea.core.communication.connector.Connector;
import org.pelea.core.communication.connector.RMIConnector;
import org.pelea.core.configuration.configuration;
import org.pelea.implementations.Execution;
import org.pelea.ros.response.response;
import org.pelea.utils.Util;
import org.ros.concurrent.CancellableLoop;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.parameter.ParameterTree;
import org.ros.node.topic.Publisher;
import org.ros.node.topic.Subscriber;
import pddl2xml.Pddl2XML;

/**
 *
 * @author momartin
 */
public class ExecutionBasic extends AbstractNodeMain implements Execution {
    
    public static byte RUNNING = 1;
    public static byte WAITING = 2;
    public static byte STOPPING = 3;
    
    public static int TIME = 1000;
    
    private String name;
    private byte mode;
    private boolean executionMode;
    private Connector commumicationModel;
    
    private long time;
    private int state;
    private String actualState;
    
    private Publisher<std_msgs.String> publisher; 
    private Subscriber<std_msgs.String> subscriber;
    private Log messages;  
    
    public ExecutionBasic() {
        
        configuration.getInstance().readConfigFile("configurationRos.xml");
        
        try {
            this.name = "EXE";
            int port = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "PORT"));
            String host = configuration.getInstance().getParameter("GENERAL", "IP");
            this.commumicationModel = (RMIConnector) new RMIConnector(host, port, Connector.CLIENT, (byte) Byte.parseByte(configuration.getInstance().getParameter(this.name, "type")), this.name);
        } catch (NotBoundException ex) {
            Logger.getLogger(ExecutionBasic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ExecutionBasic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(ExecutionBasic.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private String getMessage(String message) {
        return "[" + this.name + "]: " + message; 
    }
    
    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("rosjava/Pelea/" + this.name);
    }

    @Override
    public void executePlan(String planL) {
	try {
            XMLOutputter outputter  = new XMLOutputter(Format.getPrettyFormat());
            SAXBuilder builder      = new SAXBuilder();
            Document document       = (Document) builder.build(new StringReader(planL));
            Element plan            = ((Element) document.getRootElement());        
            List actions            = plan.getChildren();
        
            for (Object action : actions) {
                this.executeAction(outputter.outputString((Element) action));
            }
        } 
        catch (Exception e) {
            this.messages.error(this.getMessage("Executing plan of actions"));
	}
    }

    @Override
    public void executeAction(String actionL) {
        if (this.executionMode) {
            std_msgs.String message = this.publisher.newMessage();
            message.setData(actionL);            
            this.publisher.publish(message);
        }
        else {
            this.messages.info(this.getMessage("Executing action: " + actionL));
        }
    }

    @Override
    public void executeAction(double time, String action) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getSensors() {
        String xml    = ""; 

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
        xml += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
        xml += actualState;
        xml += "</define>";
        
        return xml;
    }

    @Override
    public String getSensorsWithTime(double instant_time) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getLastTime() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String solveProblem() {
        String xml = "";
        response res = new response();
        
        try 
        {
            res.addNode("domain", Pddl2XML.convertDomain(configuration.getInstance().getParameter("GENERAL", "DOMAIN")));
            res.addNode("problem", Pddl2XML.convertProblem(configuration.getInstance().getParameter("GENERAL", "PROBLEM")));
            
            xml = res.generateResponse();
        } 
        catch (Exception ex) 
        {
            xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            xml += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            xml += "<error>";
            xml += "Error getting actual state";
            xml += "</error>";
            xml += "</define>";
        }
        
        return xml;
    } 
    
    @Override
    public void onStart(final ConnectedNode connectedNode) {
        
        this.messages = connectedNode.getLog();
        this.publisher = connectedNode.newPublisher("action", std_msgs.String._TYPE);
        this.subscriber = connectedNode.newSubscriber("sensor", std_msgs.String._TYPE);

        this.subscriber.addMessageListener(new MessageListener<std_msgs.String>() {    
            @Override
            public void onNewMessage(std_msgs.String message) {
                actualState = message.getData();
            }
        });
        
        
        connectedNode.executeCancellableLoop(new CancellableLoop() {

            @Override
            protected void loop() throws InterruptedException {

                if (((RMIConnector) commumicationModel).messages()) {
                    try 
                    {
                        Message send = null;
                        Message recieve = ((RMIConnector) commumicationModel).getMessage();
                        
                        messages.info(getMessage("Reciving PELEA message " + recieve.getContent()));
                        
                        switch (recieve.getTypeMsg()) {
                            case Messages.MSG_START:
                                send = new Message(commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_SOLVEPROBLEM, solveProblem());
                                break;
                            case Messages.MSG_STOP:
                                System.exit(0);
                                break;
                            case Messages.MSG_EXECUTEPLAN:
                                executePlan(recieve.getContent());
                                send = new Message(commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, getSensors());
                                break;
                            case Messages.MSG_EXECUTEACTION:
                                executeAction(recieve.getContent());
                                send = new Message(commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, getSensors());
                                break;
                            case Messages.MSG_GETSENSORS:
                                send = new Message(commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, getSensors());
                                break;
                        }
                        
                        if (send != null) {
                            ((RMIConnector) commumicationModel).sendMessage(send);
                        }
                    } catch (RemoteException ex) {
                        Logger.getLogger(ExecutionBasic.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
                Thread.sleep(TIME);
            }
        });
    }
}
